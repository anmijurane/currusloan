﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="ProjectCurrusLoan.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">    

    <link rel="stylesheet" href="css/bootstrap-grid.css">
    <link rel="stylesheet" href="css/bootstrap-reboot.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">

    <title>Currus Loan - Inicio</title>
</head>
    <body>
        
        <nav class="Navegation_Bar">
        <div class="img_logo">
            <img src="images/Logo.png" alt="Logo Currus Loan" srcset="" height="100px">
        </div>        
        <div class="options">
            <ul>
                <li><a class="nav-link" href="">Inicio</a></li>
                <li><a class="nav-link" href="">Renta tu auto</a></li>
                <li><a class="nav-link" href="">Mi cuenta</a></li>
                <li>
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Mi cuenta
                        </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Opcion 1</a>
                        <a class="dropdown-item" href="#">Opcion 2</a>
                        <a class="dropdown-item" href="#">Opcion 3</a>
                    <div class="dropdown-divider">

                    </div>                        
                    </div>
                </li>                
            </ul>
        </div>




        <form class="form-inline">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">            
        </form>
    </nav>
        




    </body>
</html>


